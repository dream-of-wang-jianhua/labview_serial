#include "sys.h"
#include "usart.h"
#include "delay.h"
#include "adc.h"

void SendDebugData(float *Data, unsigned char ByteNum);

/*
* 硬件连接：模拟输入通道 PA0 PA1，可接受电压范围 0-3.3V
*           串口1 PA9 PA10，波特率 9600
*/

int main(void)
{ 
    float value[2] = {0};
    
    delay_init();	    	
    ADC_QuickInit(ADC1, ADC_Channel_0);
    ADC_QuickInit(ADC1, ADC_Channel_1);
    USART1_Init(9600);
    
    while(1)
    {
        //只提供了一个基本的采集发送功能，只是能用但不够好，需要根据自己的应用场景进行优化
        //按照这里的处理逻辑，每次收到发送标志，才进行电压采集，采集过程需要耗费一定时间，无法立刻回复
        //优化思路：一直（或定时）进行采集，等需要发送的时候发送最近一次的值
        //另外，建议采集到的电压量进行滤波（如滑动滤波）处理
        if(UsartSendFlag)
        {
            UsartSendFlag = 0;
            
            ADC_Start(ADC1, ADC_Channel_0, ADC_SampleTime_1Cycles5);    
            value[0] = (float)3.3*Get_AdcResult(ADC1)/4096;
            
            ADC_Start(ADC1, ADC_Channel_1, ADC_SampleTime_1Cycles5);
            value[1] = (float)3.3*Get_AdcResult(ADC1)/4096;
            
            SendDebugData(value, 8);  
        }          
    }
} 

void SendDebugData(float *Data, unsigned char ByteNum)
{
    int i;
    unsigned char *p;

    USART_SendData(USART1, 0XAA);
    while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
    USART_SendData(USART1, 0XBB);
    while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
    USART_SendData(USART1, 0XCC);
    while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
    USART_SendData(USART1, 0XDD);
    while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
 
    p = (unsigned char *)& (*Data);   //发送数据
    for(i = ByteNum; i > 0; i--)
    {
        USART_SendData(USART1, *(p + i - 1));
        while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
    }
    
    USART_SendData(USART1, 0XDD);
    while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
    USART_SendData(USART1, 0XCC);
    while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
    USART_SendData(USART1, 0XBB);
    while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
    USART_SendData(USART1, 0XAA);
    while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
}


