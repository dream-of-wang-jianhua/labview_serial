#ifndef __PWM_H
#define __PWM_H	

#include "sys.h"

typedef enum
{
    Channel1 = 1,
    Channel2 = 2,
    Channel3 = 3,
    Channel4 = 4
}PWM_Channel;

void PWM_QuickInit(TIM_TypeDef * TIMx, PWM_Channel Channelx, uint32_t Frequence, FunctionalState ENABLE_OR_DISABL);
void PWM_Change(TIM_TypeDef * TIMx, PWM_Channel Channelx, uint16_t D);
    
#endif 
