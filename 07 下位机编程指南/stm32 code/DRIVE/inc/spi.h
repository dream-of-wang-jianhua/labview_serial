#ifndef __SPI_H_
#define __SPI_H_

#include "sys.h"

void SPI_QuickInit(SPI_TypeDef * SPIx, uint8_t bits);
uint8_t SPI_Write_or_Read(SPI_TypeDef * SPIx, uint16_t Tx_Data);

#endif  
