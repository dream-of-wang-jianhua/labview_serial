#ifndef __ADC_H
#define __ADC_H	
#include "sys.h"

extern char Get_Adc_Done;

void ADC_QuickInit(ADC_TypeDef * instance, uint8_t channelx);
void ADC_Start(ADC_TypeDef * ADCx, uint8_t channelx, uint8_t speed);
u16 Get_AdcResult(ADC_TypeDef * ADCx);
float Get_Adc_Average(ADC_TypeDef * ADCx,u8 times);
 
#endif 
