#ifndef __KEY_H
#define __KEY_H

#include "stm32f10x.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_it.h"

typedef struct key
{
    u8 led_state;            //key此时的状态
    u8 key_change_bit;       //key是否改变标志位
}key_state;

extern key_state key0;

void KEY_Init(void);


#endif 
