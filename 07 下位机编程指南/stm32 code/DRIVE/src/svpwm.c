#include "svpwm.h"
#include "math.h"
#include "dma.h"

float Sin_Number[180];

uint16_t AC_V_OUT = 0;
float AC_V_Point = 26.0;

float AC_V_I = 2;
float AC_V_P = 30;

float AC_V_SumDek = 1800;
float AC_V_REF;
float AC_V_Dek;

void Create_Sin_Number()
{
    uint16_t i = 0;
    for(i = 0; i < 180; i ++)
    {
        Sin_Number[i] = sin(i * 3.14159 / 180);
    }
}

void Svpwm_Init()
{
    GPIO_InitTypeDef GPIO_InitStructure;
    TIM_OCInitTypeDef TIM_OCInitStructure;
    TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
    NVIC_InitTypeDef NVIC_InitStructure;
    TIM_BDTRInitTypeDef TIM_BDTRInitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;//CH1
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
    
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;//CH2
    GPIO_Init(GPIOA, &GPIO_InitStructure);
    
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;//CH3
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;//CH1N
    GPIO_Init(GPIOB, &GPIO_InitStructure);
    
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_14;//CH2N
    GPIO_Init(GPIOB, &GPIO_InitStructure);
    
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_15;//CH3N
    GPIO_Init(GPIOB, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;//BKIN
    GPIO_Init(GPIOB, &GPIO_InitStructure);

    GPIO_ResetBits(GPIOB, GPIO_Pin_12);
    
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);
     
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);		
    NVIC_InitStructure.NVIC_IRQChannel = TIM1_UP_IRQn;	
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;	 
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;	
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
    
    TIM_TimeBaseStructure.TIM_Period = 3999;//周期
    TIM_TimeBaseStructure.TIM_Prescaler = 0;//时钟	
    TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;//分频		
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_CenterAligned1;//计数模式		
    TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;	
    TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure);
    
    TIM_ClearFlag(TIM1, TIM_FLAG_Update);//清除计数器中断标志位
    TIM_ITConfig(TIM1,TIM_IT_Update,ENABLE);//开启计数器中断
    TIM_Cmd(TIM1, ENABLE);//使能计数器
    
    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;//PWM模式
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;//输出使能
    TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Enable;//互补输出使能 
    TIM_OCInitStructure.TIM_Pulse = 0;//比较寄存器，产生PWM
    TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;//输出通道高电平有效
    TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_High;//互补输出通道高电平有效
    TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Reset;//输出通道空闲为低电平
    TIM_OCInitStructure.TIM_OCNIdleState = TIM_OCNIdleState_Set;//互补输出通道空闲为低电平
    TIM_OC1Init(TIM1, &TIM_OCInitStructure);//初始化寄存器
    TIM_OC1PreloadConfig(TIM1, TIM_OCPreload_Enable);//输出比较使能
    TIM_OC2Init(TIM1, &TIM_OCInitStructure);//初始化寄存器
    TIM_OC2PreloadConfig(TIM1, TIM_OCPreload_Enable);//输出比较使能
    TIM_OC3Init(TIM1, &TIM_OCInitStructure);//初始化寄存器
    TIM_OC3PreloadConfig(TIM1, TIM_OCPreload_Enable);//输出比较使能
    
    /*刹车和死区结构体初始化*/
    TIM_BDTRInitStructure.TIM_OSSRState = TIM_OSSRState_Enable;
    TIM_BDTRInitStructure.TIM_OSSIState = TIM_OSSIState_Enable;
    TIM_BDTRInitStructure.TIM_LOCKLevel = TIM_LOCKLevel_1;
    TIM_BDTRInitStructure.TIM_DeadTime = 11;//死区时间为152nm
    TIM_BDTRInitStructure.TIM_Break = TIM_Break_Enable;
    TIM_BDTRInitStructure.TIM_BreakPolarity = TIM_BreakPolarity_High;
    TIM_BDTRInitStructure.TIM_AutomaticOutput = TIM_AutomaticOutput_Enable;
    TIM_BDTRConfig(TIM1, &TIM_BDTRInitStructure);
    TIM_Cmd(TIM1, ENABLE);//使能计数器	
    TIM_CtrlPWMOutputs(TIM1, ENABLE);//主输出使能
}

void TIM1_UP_IRQHandler(void)
{
    static uint16_t DC_AC_Control_X = 0;
    static uint16_t DC_AC_Control_Y = 180;
    
    if(TIM_GetITStatus(TIM1, TIM_IT_Update) != RESET)
    {	
        TIM_ClearITPendingBit(TIM1, TIM_FLAG_Update);
        if(DC_AC_Control_X == 0)
        {
            AC_V_REF = 22.8*Average_Value(ADC_DMA_Channel_0);
            AC_V_Dek = AC_V_Point - AC_V_REF;
            AC_V_OUT = (uint16_t)((AC_V_P * AC_V_Dek + AC_V_I * AC_V_SumDek));
            AC_V_SumDek += AC_V_Dek;
            
            if(AC_V_SumDek <= 0)
            {
                AC_V_SumDek = 0;
            }
            if(AC_V_SumDek >= 1950)
            {
                AC_V_SumDek = 1950;
            }
        }
        
        AC_V_OUT = 3600;
        
        if(AC_V_OUT >= 3900)
        {
            AC_V_OUT = 3900;
        }
        if(AC_V_OUT <= 100)
        {
            AC_V_OUT = 100;
        }
        
        if(DC_AC_Control_X < 180)
        {
            TIM1->CCR1 = (uint16_t)(AC_V_OUT * Sin_Number[DC_AC_Control_X]);
        }
        else
        {
            TIM1->CCR1 = 0;
        }
        
        if(DC_AC_Control_Y < 180)
        {
            TIM1->CCR2 = (uint16_t)(AC_V_OUT * Sin_Number[DC_AC_Control_Y]);
        }
        else
        {
            TIM1->CCR2 = 0;
        }
        
        DC_AC_Control_X = (DC_AC_Control_X + 1) % 360;
        DC_AC_Control_Y = (DC_AC_Control_Y + 1) % 360;
    }	
}

