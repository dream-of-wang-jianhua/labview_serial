#include "dac.h"

void DAC_QuickInit(uint16_t DAC_Channel)
{
    DAC_InitTypeDef DAC_InitType;
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    
    DAC_InitType.DAC_Trigger = DAC_Trigger_None;//不使用触发功能
    DAC_InitType.DAC_WaveGeneration=DAC_WaveGeneration_None;//不使用波形发生
    DAC_InitType.DAC_LFSRUnmask_TriangleAmplitude=DAC_LFSRUnmask_Bit0;//屏蔽幅值设置
    DAC_InitType.DAC_OutputBuffer=DAC_OutputBuffer_Disable;//关闭输出缓存
       
    switch(DAC_Channel)
    {
        case DAC_Channel_1: 
            GPIO_QuickInit(GPIOA, GPIO_Pin_4, GPIO_Mode_AIN);
            GPIO_SetBits(GPIOA,GPIO_Pin_4);
            DAC_Init(DAC_Channel_1,&DAC_InitType);//初始化DAC通道1
            DAC_Cmd(DAC_Channel_1, ENABLE);//DAC使能
            DAC_SetChannel1Data(DAC_Align_12b_R, 0);//12右对齐格式设置DAC值
            break;
        case DAC_Channel_2:
            GPIO_QuickInit(GPIOA, GPIO_Pin_5, GPIO_Mode_AIN);
            GPIO_SetBits(GPIOA,GPIO_Pin_5);
            DAC_Init(DAC_Channel_2,&DAC_InitType);//初始化DAC通道2
            DAC_Cmd(DAC_Channel_2, ENABLE);//DAC使能
            DAC_SetChannel2Data(DAC_Align_12b_R, 0);//12右对齐格式设置DAC值
            break;
        default : break;   
    }   
}

void DAC_SetData(uint16_t DAC_Channel,uint16_t Data)
{
    uint16_t Data_temp;
    Data_temp = (int)((float)Data * 4096 / 3300);
    
    switch(DAC_Channel)
    {
        case DAC_Channel_1: 
            DAC_SetChannel1Data(DAC_Align_12b_R, Data_temp); break;
        case DAC_Channel_2:
            DAC_SetChannel2Data(DAC_Align_12b_R, Data_temp); break;
        default : break;   
    }   
}


