#include "key.h"
#include "led.h"
#include "sys.h"
#include "exti.h"
#include "usart.h"
#include "adc.h"

//按键key0: PC5 

key_state key0;

void KEY_Init(void)
{        
    EXTI_QuickInit(GPIOC, GPIO_Pin_5, GPIO_Mode_IPU, EXTI_Trigger_Rising);
}

void EXTI9_5_IRQHandler(void)
{
    if(key0.key_change_bit == 0)
    {
        if(EXTI_GetITStatus(EXTI_Line5) != RESET)        //如果是 EXIT_5 触发的中断，则处理
        {        
            key0.led_state = key0.led_state >= 1 ? 0 : key0.led_state+ 1;
            key0.key_change_bit = 1;
            
            LED2 = !LED2;
          
            EXTI_ClearITPendingBit(EXTI_Line5);              //清除中断标志
        }  
    }
}

/*按键使用函数

        if(key0.key_change_bit == 1)
        {
            if((key0.led_state % 2) == 1)
            {
               
            }
            else
            {
                
            }           
            key0.key_change_bit = 0;                   
        }
        else
        {
            ;
        }
*/
