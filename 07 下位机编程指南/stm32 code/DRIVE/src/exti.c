#include "exti.h"

void EXTI_QuickInit(GPIO_TypeDef * GPIOx, uint16_t GPIO_Pin,GPIOMode_TypeDef mode,EXTITrigger_TypeDef What_EXTI_Trigger)
{
    EXTI_InitTypeDef EXTI_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;
    GPIO_InitTypeDef GPIO_InitStruct1;
    
    uint8_t GPIO_PortSourceGPIOx;
    uint8_t GPIO_PinSourcex;
    uint32_t EXTI_Linex;
           
    switch((uint32_t)GPIOx)
    {
    case (uint32_t)GPIOA: 
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
        break;
    case (uint32_t)GPIOB: 
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
        break;
    case (uint32_t)GPIOC: 
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
        break;
    case (uint32_t)GPIOD: 
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD, ENABLE);
        break;
    case (uint32_t)GPIOE: 
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOE, ENABLE);
        break;
    case (uint32_t)GPIOF: 
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOF, ENABLE);
        break;
    case (uint32_t)GPIOG: 
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOG, ENABLE);
        break;
    }

    GPIO_InitStruct1.GPIO_Mode = mode;
    GPIO_InitStruct1.GPIO_Pin = GPIO_Pin;
    GPIO_InitStruct1.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOx, &GPIO_InitStruct1);
    
    RCC_APB2PeriphClockCmd (RCC_APB2Periph_AFIO,ENABLE);
    
    switch((uint32_t) GPIOx)
    {
        case (uint32_t)GPIOA: GPIO_PortSourceGPIOx = GPIO_PortSourceGPIOA; break;
        case (uint32_t)GPIOB: GPIO_PortSourceGPIOx = GPIO_PortSourceGPIOB; break;
        case (uint32_t)GPIOC: GPIO_PortSourceGPIOx = GPIO_PortSourceGPIOC; break;
        case (uint32_t)GPIOD: GPIO_PortSourceGPIOx = GPIO_PortSourceGPIOD; break;
        case (uint32_t)GPIOE: GPIO_PortSourceGPIOx = GPIO_PortSourceGPIOE; break;
        case (uint32_t)GPIOF: GPIO_PortSourceGPIOx = GPIO_PortSourceGPIOF; break;
        case (uint32_t)GPIOG: GPIO_PortSourceGPIOx = GPIO_PortSourceGPIOG; break;
    }
    switch(GPIO_Pin)
    {
        case GPIO_Pin_0:  GPIO_PinSourcex = GPIO_PinSource0; EXTI_Linex = EXTI_Line0;
                          NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn; break;
        
        case GPIO_Pin_1:  GPIO_PinSourcex = GPIO_PinSource1; EXTI_Linex = EXTI_Line1;
                          NVIC_InitStructure.NVIC_IRQChannel = EXTI1_IRQn;break;
        
        case GPIO_Pin_2:  GPIO_PinSourcex = GPIO_PinSource2; EXTI_Linex = EXTI_Line2; 
                          NVIC_InitStructure.NVIC_IRQChannel = EXTI2_IRQn;break;
        
        case GPIO_Pin_3:  GPIO_PinSourcex = GPIO_PinSource3; EXTI_Linex = EXTI_Line3; 
                          NVIC_InitStructure.NVIC_IRQChannel = EXTI3_IRQn;break;
        
        case GPIO_Pin_4:  GPIO_PinSourcex = GPIO_PinSource4; EXTI_Linex = EXTI_Line4; 
                          NVIC_InitStructure.NVIC_IRQChannel = EXTI4_IRQn;break;
        
        case GPIO_Pin_5:  GPIO_PinSourcex = GPIO_PinSource5; EXTI_Linex = EXTI_Line5; 
                          NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;break;
        
        case GPIO_Pin_6:  GPIO_PinSourcex = GPIO_PinSource6; EXTI_Linex = EXTI_Line6;
                          NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;break;
        
        case GPIO_Pin_7:  GPIO_PinSourcex = GPIO_PinSource7; EXTI_Linex = EXTI_Line7; 
                          NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;break;
                          
        case GPIO_Pin_8:  GPIO_PinSourcex = GPIO_PinSource8; EXTI_Linex = EXTI_Line8; 
                          NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;break;
                          
        case GPIO_Pin_9:  GPIO_PinSourcex = GPIO_PinSource9; EXTI_Linex = EXTI_Line9; 
                          NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;break;
                          
        case GPIO_Pin_10: GPIO_PinSourcex = GPIO_PinSource10; EXTI_Linex = EXTI_Line10; 
                          NVIC_InitStructure.NVIC_IRQChannel = EXTI15_10_IRQn;break;
                          
        case GPIO_Pin_11: GPIO_PinSourcex = GPIO_PinSource11; EXTI_Linex = EXTI_Line11; 
                          NVIC_InitStructure.NVIC_IRQChannel = EXTI15_10_IRQn;break;
                          
        case GPIO_Pin_12: GPIO_PinSourcex = GPIO_PinSource12; EXTI_Linex = EXTI_Line12; 
                          NVIC_InitStructure.NVIC_IRQChannel = EXTI15_10_IRQn;break;
                          
        case GPIO_Pin_13: GPIO_PinSourcex = GPIO_PinSource13; EXTI_Linex = EXTI_Line13; 
                          NVIC_InitStructure.NVIC_IRQChannel = EXTI15_10_IRQn;break;
                          
        case GPIO_Pin_14: GPIO_PinSourcex = GPIO_PinSource14; EXTI_Linex = EXTI_Line14; 
                          NVIC_InitStructure.NVIC_IRQChannel = EXTI15_10_IRQn;break;
                          
        case GPIO_Pin_15: GPIO_PinSourcex = GPIO_PinSource15; EXTI_Linex = EXTI_Line15; 
                          NVIC_InitStructure.NVIC_IRQChannel = EXTI15_10_IRQn;break;
    }
    GPIO_EXTILineConfig(GPIO_PortSourceGPIOx, GPIO_PinSourcex);
    
    EXTI_InitStructure.EXTI_Line = EXTI_Linex;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = What_EXTI_Trigger;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);
    
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2); // 设置中断优先级分组2  
    
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;  //设置抢占优先级
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;         //设置子优先级
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;            //使能 IRQ 中断
    NVIC_Init(&NVIC_InitStructure);
}

void EXTI0_IRQHandler(void)
{

    
	EXTI_ClearITPendingBit(EXTI_Line0);  //清除EXTI0线路挂起位
}

// void EXTI9_5_IRQHandler(void)
//{			

//    EXTI_ClearITPendingBit(EXTI_Line0);  //清除EXTI0线路挂起位
//}


void EXTI15_10_IRQHandler(void)
{

    EXTI_ClearITPendingBit(EXTI_Line0);  //清除EXTI0线路挂起位
}
