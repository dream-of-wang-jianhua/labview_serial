#include "adc.h"
#include "delay.h"
#include "sys.h"
																   
void ADC_QuickInit(ADC_TypeDef * instance, uint8_t channelx) 
{
    ADC_InitTypeDef ADC_InitStructure;
    switch((uint32_t)instance)
    {
    case (uint32_t)ADC1: 
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
        break;
    case (uint32_t)ADC2: 
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC2, ENABLE);
        break;
    case (uint32_t)ADC3: 
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC3, ENABLE);
        break;
    }
    
    switch(channelx) {
    case 0x00: GPIO_QuickInit(GPIOA, GPIO_Pin_0, GPIO_Mode_AIN); break;
    case 0x01: GPIO_QuickInit(GPIOA, GPIO_Pin_1, GPIO_Mode_AIN); break;
    case 0x02: GPIO_QuickInit(GPIOA, GPIO_Pin_2, GPIO_Mode_AIN); break;
    case 0x03: GPIO_QuickInit(GPIOA, GPIO_Pin_3, GPIO_Mode_AIN); break;
    case 0x04: GPIO_QuickInit(GPIOA, GPIO_Pin_4, GPIO_Mode_AIN); break;
    case 0x05: GPIO_QuickInit(GPIOA, GPIO_Pin_5, GPIO_Mode_AIN); break;
    }
    
    RCC_ADCCLKConfig(RCC_PCLK2_Div6);
    
    ADC_DeInit(instance);
    
    ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;//ADC工作在独立模式还是其他模式
    ADC_InitStructure.ADC_ScanConvMode = DISABLE;//是否开启扫描模式
    ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;//是否开启连续转换模式
    ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;//不开启由外部触发启动，而是由软件触发启动
    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;//ADC数据右对齐
    ADC_InitStructure.ADC_NbrOfChannel = 1;//顺序进行规则转化的ADC通道数目
    ADC_Init(instance, &ADC_InitStructure);
    
    ADC_Cmd(instance, ENABLE);//使能指定的ADC
    
    ADC_ResetCalibration(instance);//使能ADc复位校准
    while(ADC_GetResetCalibrationStatus(instance));//等待复位校准完成
    ADC_StartCalibration(instance);//开启AD校准
    while(ADC_GetCalibrationStatus(instance));//等待AD校准完成
}

void ADC_Start(ADC_TypeDef * ADCx, uint8_t channelx, uint8_t speed)
{
    ADC_RegularChannelConfig(ADCx, channelx, 1, speed);
    ADC_SoftwareStartConvCmd(ADCx, ENABLE);
//    ADC_RegularChannelConfig(ADC1, ADC_Channel_2, 1, ADC_SampleTime_1Cycles5);
//    ADC_SoftwareStartConvCmd(ADC1, ENABLE);
}    

u16 Get_AdcResult(ADC_TypeDef * ADCx)   
{  
	while(!ADC_GetFlagStatus(ADCx, ADC_FLAG_EOC)); //等待转换结束
	return ADC_GetConversionValue(ADCx);	       //返回最近一次ADC规则组的转换结果
}

float Get_Adc_Average(ADC_TypeDef * ADCx,u8 times)
{
	float temp_val=0;
	u8 t;
	for(t=0;t<times;t++)
	{
		temp_val+=Get_AdcResult(ADCx);
		delay_ms(5);
	}
	return 3.3*temp_val/times/4096;
}
