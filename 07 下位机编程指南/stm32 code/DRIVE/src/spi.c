#include "spi.h"

void SPI_QuickInit(SPI_TypeDef * SPIx, uint8_t bits)
{
    SPI_InitTypeDef  SPI_InitStructure;
    if(SPIx == SPI1)
    {
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);	
        GPIO_QuickInit(GPIOA, GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7, GPIO_Mode_AF_PP); 
        PAout(5) = 1;
        PAout(6) = 1;
        PAout(7) = 1;
    }
    if(SPIx == SPI2)
    {
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2, ENABLE);	
        GPIO_QuickInit(GPIOB, GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15, GPIO_Mode_AF_PP); 
        PBout(13) = 1;
        PBout(14) = 1;
        PBout(15) = 1;
    }
    if(SPIx == SPI3)
    {
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI3, ENABLE);	
        GPIO_QuickInit(GPIOB, GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5, GPIO_Mode_AF_PP); 
        PBout(3) = 1;
        PBout(4) = 1;
        PBout(5) = 1;
    }
    SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
    SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
    if(bits == 8)
    {
        SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
    }
    if(bits == 16)
    {
        SPI_InitStructure.SPI_DataSize = SPI_DataSize_16b;
    }
    SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;
    SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
    SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
    SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_2;
    SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
    SPI_InitStructure.SPI_CRCPolynomial = 7;
    SPI_Init(SPIx, &SPI_InitStructure);
    SPI_Cmd(SPIx, ENABLE);
    
    assert_param(IS_SPI_BAUDRATE_PRESCALER(SPI_BaudRatePrescaler));
    SPIx->CR1 &= 0XFFC7;
    SPIx->CR1 |= SPI_BaudRatePrescaler_2;//����SPI�ٶ�Ϊ18M
    SPI_Cmd(SPIx, ENABLE);
}
uint8_t SPI_Write_or_Read(SPI_TypeDef * SPIx, uint16_t Tx_Data)
{
    u8 retry = 0;
    while(SPI_I2S_GetFlagStatus(SPIx, SPI_I2S_FLAG_TXE) == RESET)
    {
        retry ++;
        if(retry>200)
        {
            return 0;
        }
    }
    SPI_I2S_SendData(SPIx, Tx_Data);
    retry = 0;
    while(SPI_I2S_GetFlagStatus(SPIx, SPI_I2S_FLAG_RXNE) == RESET)
    {
        retry ++;
        if(retry > 200)
        {
            return 0;
        }
    }
    return SPI_I2S_ReceiveData(SPIx);
}
